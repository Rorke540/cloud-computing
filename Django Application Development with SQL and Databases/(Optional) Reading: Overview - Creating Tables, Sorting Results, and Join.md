## (Optional) Reading: Overview - Creating Tables, Sorting Results, and Join

There are many ways that you can use SQL to help analyze your data and present it in a way that supports your data story. In this bonus lesson, you will learn some SQL techniques beyond the basics, that will help you do more with data. You’ll learn how to use SQL commands to create tables. You’ll also learn how to refine your query results, as well as sort and organize your results, making your query results easier to take useful insights from.

The following optional videos are provided for informational purposes only and do not contain any graded content. While useful to know these things about SQL, those developing applications will typically employ other mechanisms to perform these tasks, some of which will be taught in later modules of this course.

If you want to learn more about SQL beyond what is covered in this course, the following course will be of interest, especially for those in Data Engineering, Data Science, or Data Analyst tracks:

[SQL for Data Science](https://www.edx.org/course/sql-for-data-science)
