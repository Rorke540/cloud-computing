## Introduction

### Overview

This final project will encourage you to use many of the tools and concepts that you have learned so far in this course. As you complete the project, you will need to take screenshots of your work to verify what you have done so that it can be graded. The final project is conducted on an OpenShift cluster. 

### Build and Deploy a Simple Health Application

The health application is a simple, multi-tier web application that we will build and deploy with Docker and Kubernetes. The application consists of a web front end and an adapter that serves data from a Cloudant database to the front end application. For both of these we will create Kubernetes Deployments, Pods, and Services. We will deploy and manage this entire application on OpenShift.

### Review Criteria

After completing the Hands-on Lab: Build and Deploy a Simple Health App, you will complete the Peer-graded Assignment and be graded on the following 9 tasks.

For each of the 9 tasks you will be required to provide a screenshot and upload the JPEG (.jpg) file for your peers to review. This project is worth 20% of your total grade.

Task 1: Deploy a simple health app.

Task 2: Use the health app in demo mode.

Task 3: Update the simple health app login page to include your name.

Task 4: Automatically deploy the text update using a second image stream tag.

Task 5: Redeploy the health app using an OpenShift build.

Task 6: Deploy the patient database app.

Task 7: Use Cloudant for the health app instead of an in-memory datastore.

Task 8: Log in to the health app using a username and password combination from the database.

Task 9: Create a Horizontal Pod Autoscaler that targets the patient-ui Deployment and autoscales it to three replicas.
