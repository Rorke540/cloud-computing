## Next Steps and Resources

Now that you have a foundational understanding of Containers, Kubernetes, and OpenShift, you can develop your skills further by taking one of the following courses:

- [Introduction to OpenShift Applications](https://www.redhat.com/en/services/training/do101-introduction-openshift-applications?section=Overview)
- [Getting started with Microservices with Istio and IBM Cloud Kubernetes Service](https://cognitiveclass.ai/courses/get-started-with-microservices-istio-and-ibm-cloud-container-service)


After completing this course, you may be ready for your first IBM Cloud certification: [Cloud Solution Advisor](https://www.ibm.com/training/path/41/ibmcloudassociatesolutionadvisor).

Continue your cloud learning journey with role-based and product-based training and certifications at the [IBM Center for Cloud Training](https://www.ibm.com/training/cloud).
